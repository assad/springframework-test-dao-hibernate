package site.assad.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.assad.dao.UserDao;
import site.assad.domain.User;

import java.util.List;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2017/12/15 19:12
 * Description:
 */
@Service
@Transactional
public class UserService {

    @Autowired
    private UserDao userDao;

    //检查某个名称的user是否存在
    public boolean checkUser(final String username){
        return userDao.findUserByName(username) != null;
    }

    //删除指定icon的user
    public void deleteUserWithIcon(final String icon){
        List<User> userList = userDao.findUserByIcon(icon);
        for(User user : userList)
            userDao.deleteUser(user);
    }




}
