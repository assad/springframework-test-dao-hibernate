package site.assad.service;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import site.assad.dao.UserDao;

import static org.junit.Assert.assertTrue;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2017/12/16 16:18
 * Description:
 */
//@ContextConfiguration
public class UserServcieTest {

    private UserService userServcie;
    @Before
    public void init(){
        userServcie = new ClassPathXmlApplicationContext("site/assad/applicationContext.xml").getBean("userService",UserService.class);
    }

    @Test
    public void testCheckUser(){
        boolean flag =  userServcie.checkUser("assad");
        assertTrue(flag);
    }



}
