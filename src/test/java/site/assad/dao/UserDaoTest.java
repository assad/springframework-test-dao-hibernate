package site.assad.dao;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import site.assad.domain.User;

import static org.junit.Assert.assertNotNull;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2017/12/16 17:05
 * Description:
 */
public class UserDaoTest {

    private UserDao userDao ;

    @Before
    public void init(){
        userDao = new ClassPathXmlApplicationContext("site/assad/applicationContext.xml").getBean("userDao",UserDao.class);
    }

    @Test
    public void testFindUserByName(){
        User user = userDao.findUserByName("assad");
        assertNotNull(user);
    }
}
